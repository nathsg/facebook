import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import PersistentDrawerLeft from './Drawer';


const useStyles = makeStyles(theme => ({
   root: {
     flexGrow: 1,
   },
 }));

const App = () => {
  const classes = useStyles();
  
  return (
   <div className={classes.root}>
      <AppBar position="static">
         < PersistentDrawerLeft />
      </AppBar>
   </div>
   )
}
export default App;