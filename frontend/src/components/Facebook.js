import React from 'react';
import { Link } from 'react-router-dom'

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import  { mdiFacebookBox } from '@mdi/js';
import Icon from '@mdi/react';

export default function Facebook() {
    const [open, setOpen] = React.useState(false);

    function handleOpen() {
        setOpen(true);
    }

    function handleClose() {
      setOpen(false);
    }

    return (
        <List>
        <ListItem button onClick={open ? handleClose : handleOpen}>
              <ListItemIcon>
              <Icon path={mdiFacebookBox}
                size={1.2}/>
              </ListItemIcon>
              <ListItemText primary={'Facebook'} />
              {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
        </List>
    )
}

