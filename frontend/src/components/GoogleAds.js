import React from 'react';

import { Link } from 'react-router-dom'

import List from '@material-ui/core/List';
import Collapse from '@material-ui/core/Collapse';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import  { mdiHome, mdiGoogleAdwords, mdiViewList } from '@mdi/js';
import Divider from '@material-ui/core/Divider';
import Icon from '@mdi/react';
import { makeStyles, useTheme } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    nested: {
      paddingLeft: theme.spacing(4),
    },
  })
);

export default function GoogleAds() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  function handleOpen() {
      setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <List>
      <ListItem button onClick={open ? handleClose : handleOpen}>
        {console.log(open)}
        <ListItemIcon>
            <Icon path={mdiGoogleAdwords}
                size={1.1}/>
        </ListItemIcon>
        <ListItemText primary={'Google Ads'} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
            <Icon path={mdiViewList}
                size={1.1}/>
            </ListItemIcon>
            <ListItemText inset primary="Campaign" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
            <Icon path={mdiHome}
                size={1.1}/>
            </ListItemIcon>
            <ListItemText inset primary="Account" />
          </ListItem>
        </List>
      </Collapse>
    </List>
  )
}

