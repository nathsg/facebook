def get_campaign(my_account):
    fields = [
        'name',
        'account_id',
        'objective',
        'status',
        'daily_budget',
        'budget_remaining',
        'lifetime_budget'
    ]
    params = {
    'effective_status': ['ACTIVE','PAUSED'],
    }
    return my_account.get_campaigns(fields=fields,params=params)  #returns campaing id
