def create_ad_set(my_account):
    fields = []
    params = {
        'name': 'My First AdSet',
        'lifetime_budget': '20000',
        'start_time': '2019-05-06T08:44:23-0700',
        'end_time': '2020-05-13T08:44:23-0700',
        'campaign_id': '23843443222940092',
        'bid_amount': '500',
        'billing_event': 'IMPRESSIONS',
        'optimization_goal': 'POST_ENGAGEMENT',
        'targeting': 
            {'age_min':20,
            'age_max':24,
            'geo_locations':{'countries':['US'],},
            },
        'status': 'PAUSED',
    }
    my_account.create_ad_set(fields=fields,params=params)