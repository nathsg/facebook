def create_campaign(my_account, name, objective):
    fields = []
    params = {
        'name': name,
        'objective': objective, #limited
        'status': 'PAUSED',
    }
    return my_account.create_campaign(fields=fields,params=params)  #returns campaing id
